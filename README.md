# trivy-db-glad

Build pipeline for [Trivy](https://github.com/aquasecurity/trivy) databases that
contain the [GitLab Advisory Database](https://gitlab.com/gitlab-org/security-products/gemnasium-db).

## Pulling a database artifact

Databases are stored as OCI artifacts in the container registry and can be
pulled with e.g. the [ORAS CLI](https://oras.land/):

```
oras pull -a registry.gitlab.com/gitlab-org/protect/demos/sandbox/trivy-db-gitlab-build:2
```

## Available tags

Mirroring [trivy-db](https://github.com/aquasecurity/trivy-db)'s tagging scheme
the latest database artifact is tagged with `:2`. All artifacts are additionally
accessible via a `:YYYYMMDDHHmm` tag. For example:

* registry.gitlab.com/gitlab-org/protect/demos/sandbox/trivy-db-gitlab-build:2
* registry.gitlab.com/gitlab-org/protect/demos/sandbox/trivy-db-gitlab-build:2022041317
